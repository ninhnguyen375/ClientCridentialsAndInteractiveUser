﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using IdentityApi.Data;
using IdentityApi.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace IdentityApi {
    public class Startup {
        public IWebHostEnvironment Environment { get; }

        public Startup (IWebHostEnvironment environment) {
            Environment = environment;
        }

        public void ConfigureServices (IServiceCollection services) {
            // uncomment, if you want to add an MVC-based UI
            services.AddControllersWithViews ();

            services.AddDbContext<ApplicationDbContext> (options =>
                options.UseSqlite ("Data Source=App.db"));
            services.AddIdentity<ApplicationUser, IdentityRole> ()
                .AddEntityFrameworkStores<ApplicationDbContext> ()
                .AddDefaultTokenProviders ();

            var builder = services.AddIdentityServer (options => {
                    options.Events.RaiseErrorEvents = true;
                    options.Events.RaiseInformationEvents = true;
                    options.Events.RaiseFailureEvents = true;
                    options.Events.RaiseSuccessEvents = true;
                })
                .AddInMemoryIdentityResources (Config.Ids)
                .AddInMemoryApiResources (Config.Apis)
                .AddInMemoryClients (Config.Clients)
                .AddAspNetIdentity<ApplicationUser> ();

            // not recommended for production - you need to store your key material somewhere secure
            builder.AddDeveloperSigningCredential ();
        }

        public void Configure (IApplicationBuilder app) {
            if (Environment.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            }

            // uncomment if you want to add MVC
            app.UseStaticFiles ();
            app.UseRouting ();

            app.UseIdentityServer ();

            // uncomment, if you want to add MVC
            app.UseAuthorization ();
            app.UseEndpoints (endpoints => {
                endpoints.MapDefaultControllerRoute ();
            });
        }
    }
}