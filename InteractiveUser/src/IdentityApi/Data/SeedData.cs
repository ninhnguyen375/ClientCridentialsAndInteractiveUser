using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityApi.Models;
using IdentityModel;
using Microsoft.AspNetCore.Identity;
using Serilog;

namespace IdentityApi.Data {
    public class SeedData {
        public static async Task InitializeAync (ApplicationDbContext context, UserManager<ApplicationUser> userManager) {
            context.Database.EnsureCreated ();
            var user = await userManager.FindByNameAsync ("ninh");
            if (user == null) {
                user = new ApplicationUser { UserName = "ninh" };
                var result = userManager.CreateAsync (user, "Ninh123.").Result;
                if (!result.Succeeded) {
                    throw new Exception (result.Errors.First ().Description);
                }

                result = userManager.AddClaimsAsync (user, new Claim[] {
                    new Claim (JwtClaimTypes.Name, "Ninh"),
                        new Claim (JwtClaimTypes.GivenName, "Ninh")
                }).Result;
                if (!result.Succeeded) {
                    throw new Exception (result.Errors.First ().Description);
                }
                Log.Debug ("Seed account created");
            }
        }

    }
}