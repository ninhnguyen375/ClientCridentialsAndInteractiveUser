﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;

namespace IdentityApi {
    public static class Config {
        public static IEnumerable<IdentityResource> Ids =>
            new List<IdentityResource> {
                new IdentityResources.OpenId (),
                new IdentityResources.Profile ()
            };

        public static IEnumerable<ApiResource> Apis =>
            new List<ApiResource> {
                new ApiResource ("movie", "Movie API")
            };

        public static IEnumerable<Client> Clients =>
            new List<Client> {
                new Client {
                ClientId = "mvc",
                AlwaysIncludeUserClaimsInIdToken = true,
                ClientSecrets = { new Secret ("secret".Sha256 ()) },

                AllowedGrantTypes = GrantTypes.Code,
                RequireConsent = false,
                RequirePkce = true,

                RedirectUris = { "http://localhost:5002/signin-oidc" },
                PostLogoutRedirectUris = { "http://localhost:5002/signout-callback-oidc" },

                AllowedScopes = new List<string> {
                IdentityServerConstants.StandardScopes.OpenId,
                IdentityServerConstants.StandardScopes.Profile,
                "movie"
                },

                AllowOfflineAccess = true,
                }
            };

    }
}