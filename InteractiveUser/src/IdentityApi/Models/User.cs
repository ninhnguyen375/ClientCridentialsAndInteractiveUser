using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace IdentityApi.Models {
    public class User {
        [Key]
        public string SubjectId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}