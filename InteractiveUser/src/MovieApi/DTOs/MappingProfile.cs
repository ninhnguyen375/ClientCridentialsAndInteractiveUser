using MovieApi.Models;

namespace MovieApi.DTOs
{
    public static class MappingProfile
    {
        public static MovieDTO ToDTO(this Movie movie)
        {
            return new MovieDTO
            {
                Id = movie.Id,
                Title = movie.Title,
                ReleaseDate = movie.ReleaseDate,
                Genre = movie.Genre,
                Price = movie.Price,
                Rating = movie.Rating
            };
        }

        public static Movie ToMovie(this MovieDTO movieDTO)
        {
            return new Movie
            {
                Id = movieDTO.Id,
                Title = movieDTO.Title,
                ReleaseDate = movieDTO.ReleaseDate,
                Genre = movieDTO.Genre,
                Price = movieDTO.Price,
                Rating = movieDTO.Rating
            };
        }

        public static void MapTo(this MovieDTO movieDTO, Movie movie)
        {
            movie.Id = movieDTO.Id;
            movie.Title = movieDTO.Title;
            movie.ReleaseDate = movieDTO.ReleaseDate;
            movie.Genre = movieDTO.Genre;
            movie.Price = movieDTO.Price;
            movie.Rating = movieDTO.Rating;
        }
    }
}