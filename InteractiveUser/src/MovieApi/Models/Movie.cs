using System;

namespace MovieApi.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public decimal Price { get; internal set; }
        public string Genre { get; set; }
        public string Rating { get; internal set; }
        public string Secret { get; set; }
    }
}