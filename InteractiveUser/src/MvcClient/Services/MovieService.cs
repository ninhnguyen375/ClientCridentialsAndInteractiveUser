using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MvcClient.Infrastructure;
using MvcClient.Models;

namespace MvcClient.Services
{
    public class MovieService : IMovieService
    {
        private readonly string _baseUrl;
        private readonly IHttpClient _httpClient;

        public MovieService(IHttpClient httpClient, IOptions<AppSettings> appSettings)
        {
            _httpClient = httpClient;
            _baseUrl = appSettings.Value.MovieUrl;
        }

        public async Task<IEnumerable<Movie>> GetMovies()
        {
            var uri = _baseUrl;

            return await _httpClient.GetListAsync<Movie>(uri);
        }

        public async Task<Movie> GetMovie(int id)
        {
            var uri = _baseUrl + $"/{id}";

            return await _httpClient.GetAsync<Movie>(uri);
        }

        public async Task CreateMovie(Movie movie)
        {
            var uri = _baseUrl;

            await _httpClient.PostAsync(uri, movie);
        }

        public async Task UpdateMovie(int id, Movie movie)
        {
            var uri = _baseUrl + $"/{id}";

            await _httpClient.PutAsync(uri, movie);
        }

        public async Task DeleteMovie(int id)
        {
            var uri = _baseUrl + $"/{id}";

            await _httpClient.DeleteAsync(uri);
        }
    }
}