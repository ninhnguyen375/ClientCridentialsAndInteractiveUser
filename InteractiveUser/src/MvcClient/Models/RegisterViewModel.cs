using System.ComponentModel.DataAnnotations;

namespace MvcClient.Models
{
    public class RegisterViewModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }
        public bool RememberMe { get; set; }
    }
}