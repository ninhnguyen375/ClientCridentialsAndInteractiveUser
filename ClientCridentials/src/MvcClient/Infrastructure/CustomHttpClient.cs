using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace MvcClient.Infrastructure
{
    public class CustomHttpClient : IHttpClient
    {
        private readonly HttpClient _httpClient;
        private readonly AppSettings _appSettings;

        public CustomHttpClient(HttpClient httpClient, IOptions<AppSettings> appSettings)
        {
            _httpClient = httpClient;
            _appSettings = appSettings.Value;
        }

        public async Task<IEnumerable<T>> GetListAsync<T>(string uri) where T : class
        {
            _httpClient.SetBearerToken(await GetAccessToken());

            var responseString = await _httpClient.GetStringAsync(uri);

            return JsonConvert.DeserializeObject<IEnumerable<T>>(responseString);

        }

        public async Task<T> GetAsync<T>(string uri) where T : class
        {
            _httpClient.SetBearerToken(await GetAccessToken());

            var responseString = await _httpClient.GetStringAsync(uri);

            return JsonConvert.DeserializeObject<T>(responseString);

        }

        public async Task PostAsync(string uri, object entity)
        {
            var content = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json");

            _httpClient.SetBearerToken(await GetAccessToken());

            var response = await _httpClient.PostAsync(uri, content);

            if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new Exception($"Error in Creating {entity.GetType().Name}, try later");
            }

            response.EnsureSuccessStatusCode();
        }

        public async Task PutAsync(string uri, object entity)
        {
            var content = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json");

            _httpClient.SetBearerToken(await GetAccessToken());

            var response = await _httpClient.PutAsync(uri, content);

            if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new Exception("Error in Udpating {entity.GetType().Name}, try later");
            }

            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteAsync(string uri)
        {
            _httpClient.SetBearerToken(await GetAccessToken());

            var response = await _httpClient.DeleteAsync(uri);

            if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new Exception($"Error in deleting object, try later");
            }

            response.EnsureSuccessStatusCode();
        }

        private async Task<string> GetAccessToken()
        {
            var disco = await this._httpClient.GetDiscoveryDocumentAsync(_appSettings.IdentityUrl);

            var tokenResponse = await this._httpClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,

                ClientId = _appSettings.ClientCredentials.ClientId, 
                ClientSecret =_appSettings.ClientCredentials.ClientSecret, 
                Scope = _appSettings.ClientCredentials.Scope, 
            });

            return tokenResponse.AccessToken;
        }
    }
}