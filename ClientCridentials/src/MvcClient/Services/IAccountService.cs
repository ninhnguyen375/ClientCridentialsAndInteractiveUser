using System.Collections.Generic;
using System.Threading.Tasks;
using MvcClient.Models;

namespace MvcClient.Services {
    public interface IAccountService {
        Task<User> Login (string username, string password);
        Task Register (string username, string password);
    }
}