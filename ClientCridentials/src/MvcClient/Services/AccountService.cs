using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MvcClient.Data;
using MvcClient.Infrastructure;
using MvcClient.Models;

namespace MvcClient.Services {
    public class AccountService : IAccountService {
        private ApplicationDbContext _context;
        public AccountService (ApplicationDbContext context) {
            _context = context;
        }

        public async Task<User> Login (string username, string password) {
            var user = await _context.Users.FirstOrDefaultAsync (
                u => u.Username == username
                && u.Password == password);
            return user;
        }

        public async Task Register (string username, string password) {
            User user = new User { Username = username, Password = password };
            await _context.Users.AddAsync (user);
        }
    }
}