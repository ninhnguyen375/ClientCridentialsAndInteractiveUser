using System.Collections.Generic;
using System.Threading.Tasks;
using MvcClient.Models;

namespace MvcClient.Services
{
    public interface IMovieService
    {
        Task<IEnumerable<Movie>> GetMovies();
        Task<Movie> GetMovie(int id);
        Task CreateMovie(Movie movie);
        Task UpdateMovie(int id, Movie movie);
        Task DeleteMovie(int id);
    }
}