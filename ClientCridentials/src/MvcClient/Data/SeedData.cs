using System;
using System.Linq;
using System.Threading.Tasks;
using MvcClient.Models;

namespace MvcClient.Data {
    public class SeedData {
        public static async Task InitializeAync (ApplicationDbContext context) {
            context.Database.EnsureCreated ();

            if (!context.Users.Any ()) {
                await context.Users.AddRangeAsync (
                    new User { Id = 1, Username = "Ninh", Password = "12345678" }
                );

                await context.SaveChangesAsync ();
            }
        }

    }
}