using Microsoft.EntityFrameworkCore;
using MvcClient.Models;

namespace MvcClient.Data {
    public class ApplicationDbContext : DbContext {
        public ApplicationDbContext (DbContextOptions<ApplicationDbContext> options) : base (options) { }

        public DbSet<User> Users { get; set; }
    }
}