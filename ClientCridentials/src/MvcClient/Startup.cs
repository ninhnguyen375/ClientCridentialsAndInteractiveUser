using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MvcClient.Data;
using MvcClient.Infrastructure;
using MvcClient.Services;

namespace MvcClient {
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices (IServiceCollection services) {
            services.Configure<AppSettings> (Configuration);
            services.AddHttpClient<IHttpClient, CustomHttpClient> ();
            services.AddScoped<IMovieService, MovieService> ();
            services.AddScoped<IAccountService, AccountService> ();

            services.AddControllersWithViews ();

            services.AddDbContext<ApplicationDbContext> (options => options.UseSqlite ("Data Source=App.db"));

            services.AddAuthentication ("Cookies")
                .AddCookie ("Cookies");
        }

        public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            } else {
                app.UseExceptionHandler ("/Home/Error");
            }

            app.UseStaticFiles ();

            app.UseRouting ();
            app.UseAuthentication ();
            app.UseAuthorization ();

            app.UseEndpoints (endpoints => {
                endpoints.MapDefaultControllerRoute ();
            });
        }
    }
}