using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MvcClient.Data;

namespace MvcClient {
    public class Program {
        public static void Main (string[] args) {
            var host = CreateHostBuilder (args).Build ();

            using (var scope = host.Services.CreateScope ()) {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<ApplicationDbContext> ();
                try {
                    SeedData.InitializeAync (context).Wait ();
                } catch (Exception ex) {
                    var logger = services.GetRequiredService<ILogger<Program>> ();
                    logger.LogError ("An error occured seeding database.", ex);
                }
            }

            host.Run ();
        }

        public static IHostBuilder CreateHostBuilder (string[] args) =>
            Host.CreateDefaultBuilder (args)
            .ConfigureWebHostDefaults (webBuilder => {
                webBuilder.UseStartup<Startup> ();
            });
    }
}