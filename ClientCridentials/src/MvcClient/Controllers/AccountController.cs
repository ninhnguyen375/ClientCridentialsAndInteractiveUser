using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MvcClient.Models;
using MvcClient.Services;

namespace MvcClient.Controllers {
    public class AccountController : Controller {
        private readonly IAccountService _service;

        public AccountController (IAccountService accountService) {
            _service = accountService;
        }

        [HttpPost]
        public async Task<IActionResult> Login (LoginViewModel model) {
            if (_service.Login (model.Username, model.Password) == null)
                return View ();

            // create claims
            List<Claim> claims = new List<Claim> {
                new Claim (ClaimTypes.Name, model.Username)
            };

            // create identity
            ClaimsIdentity identity = new ClaimsIdentity (claims, "cookie");

            // create principal
            ClaimsPrincipal principal = new ClaimsPrincipal (identity);

            // sign-in
            await HttpContext.SignInAsync (
                scheme: "Cookies",
                principal : principal,
                properties : new AuthenticationProperties { });

            return Redirect (model.ReturnUrl ?? "/");
        }

        [HttpGet]
        public IActionResult Login () {
            return View ();
        }

        public async Task<IActionResult> Logout (string requestPath) {
            await HttpContext.SignOutAsync (
                scheme: "Cookies");

            return RedirectToAction ("Login");
        }

        [HttpGet]
        public IActionResult Register () {
            return View ();
        }

        [HttpPost]
        public async Task<IActionResult> Register (RegisterViewModel model) {
            if (model.ConfirmPassword != model.Password)
                return View ();
            await _service.Register (model.Username, model.Password);
            return RedirectToAction ("Login");
        }
    }
}